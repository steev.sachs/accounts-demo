# Accounts demo
This is a very basic demo accounts service and frontend.

## Run Service
`yarn start-server`

## Run App
`yarn start`

## Run Tests
`yarn test`
