module.exports = {
  coveragePathIgnorePatterns: ['<rootDir>/node_modules/'],
  setupFilesAfterEnv: ['<rootDir>/scripts/setupTests.js'],
  testEnvironment: 'jsdom-global',
  testRegex: 'src/.*(/__tests__/[^.]+.(?!integration)|\\.(test|spec))\\.{t,j}sx?$',
  transform: {
    '\\.tsx?$': 'babel-jest'
  }
};
