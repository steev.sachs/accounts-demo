import AccountManager from './components/AccountManager';
import React from 'react';
import { css } from 'emotion';

const styles = {
  container: css({
    display: 'flex',
  })
};

const App = () => (
  <div className={styles.container}>
    <AccountManager />
  </div>
);

export default App;
