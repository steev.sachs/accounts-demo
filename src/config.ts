type Config = {
  serviceUrl: string
}

const config: Config = {
  serviceUrl: process.env.SERVICE_URL || 'http://localhost:4000'
};

export default config;
