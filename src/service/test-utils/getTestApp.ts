import Application from '../app';

export function getTestApp() {
  const app = new Application().getApp();
  const server = app.listen();
  return server;
}
