let id = 1;

const mockAccounts = [
  {
    birthday: new Date('1986-12-31'),
    emailAddress: 'test1@test.com',
    id: '' + id++,
    name: 'Tester One'
  },
  {
    birthday: new Date('1984-02-12'),
    emailAddress: 'test2@test.com',
    id: '' + id++,
    name: 'Tester Two'
  },
  {
    birthday: new Date('1989-10-31'),
    emailAddress: 'test3@test.com',
    id: '' + id++,
    name: 'Tester Three'
  },
  {
    birthday: new Date('1990-11-25'),
    emailAddress: 'test4@test.com',
    id: '' + id++,
    name: 'Tester Four'
  },
  {
    birthday: new Date('1986-01-01'),
    emailAddress: 'test5@test.com',
    id: '' + id++,
    name: 'Tester Five'
  }
];

export default mockAccounts;
