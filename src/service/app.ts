import Koa from 'koa';
import checkOrigin from './utils/checkOrigin';
import cors from '@koa/cors'
import handleError from './middleware/handleError';
import morgan from 'koa-morgan';
import router from './routes';

export default class Application {
  app: Koa | undefined;

  constructor() {
    this.app = undefined;
  }

  getApp(): Koa {
    if (this.app) {
      return this.app;
    }

    this.app = new Koa();
    if (process.env.NODE_ENV !== 'test') {
      this.app.use(morgan(process.env.NODE_ENV === 'production' ? 'combined' : 'dev'));
    }

    this.app.use(cors({ credentials: true, origin: checkOrigin }))
    this.app.use(handleError);

    this.app.use(router.routes());
    this.app.use(router.allowedMethods());

    return this.app;
  }
}
