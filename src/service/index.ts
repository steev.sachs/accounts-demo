import { createServer as createHttpServer } from 'http';
import Application from './app';
import Koa from 'koa';

const app: Koa = new Application().getApp();

const server = createHttpServer(app.callback());

const port = process.env.PORT || 4000;
server.listen(port, () => {
  console.info(`Application listening on port ${port}`);
});
