import { Ctx, User } from '../../types';
import { find, findIndex, propEq } from 'ramda';
import cuid from 'cuid';
import db from '../db/memory';

const getAllAccounts = async (_: Ctx) => {
  return db;
};

const deleteAccount = async (ctx: Ctx, { id }: { id: string }) => {
  const allAccounts = await getAllAccounts(ctx);
  const indexToDelete = findIndex(propEq('id', id), allAccounts);

  return allAccounts.splice(indexToDelete, 1);
};

const getAccountById = async (ctx: Ctx, { id }: { id: string }) => {
  const allAccounts = await getAllAccounts(ctx);
  const account = find(propEq('id', id), allAccounts);

  return account;
};

const insertAccount = async (_: Ctx, { account }: { account: User }) => {
  const id = account.id || cuid();
  const newAccount = { ...account, id };
  db.push(newAccount);

  return newAccount;
};

const updateAccount = async (ctx: Ctx, { id, ...updates }: Partial<User>) => {
  const allAccounts = await getAllAccounts(ctx);
  const indexToUpdate = findIndex(propEq('id', id), allAccounts);
  db[indexToUpdate] = { ...db[indexToUpdate], ...updates };

  return db[indexToUpdate];
};

export default {
  deleteAccount,
  getAccountById,
  getAllAccounts,
  insertAccount,
  updateAccount,
};
