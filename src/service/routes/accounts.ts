import { User } from '../../types';
import {
  deleteAccount,
  getAccountById,
  getAllAccounts,
  insertAccount,
  searchAccounts,
  updateAccount
} from '../services/accounts';
import { filter } from 'ramda'
import Router from 'koa-router';
import koaBody from 'koa-body';

const router = new Router();

router.get('/', async (ctx) => {
  const { any, emailAddress, id, name } = ctx.request.query;
  if (any || emailAddress || id || name) {
    const accounts = await searchAccounts(ctx, filter(Boolean, { any, emailAddress, id, name }));
    ctx.status = 200;
    ctx.body = {
      accounts
    };
  } else {
    const accounts = await getAllAccounts(ctx);
    ctx.status = 200;
    ctx.body = {
      accounts
    };
  }
});

router.post('/', koaBody(), async (ctx) => {
  const account: User = ctx.request.body;
  if (!account.birthday || !account.emailAddress || !account.name) {
    return ctx.throw(400, 'birthday, emailAddress, and name are required properties');
  }

  const newAccount = await insertAccount(ctx, { account });

  ctx.status = 200;
  ctx.body = {
    account: newAccount
  };
});

router.get('/:id', async (ctx) => {
  const id = ctx.params.id;

  const account = await getAccountById(ctx, { id });
  if (!account) {
    return ctx.throw(404, `No account found with id ${id}`);
  }

  ctx.status = 200;
  ctx.body = {
    account
  };
});

router.delete('/:id', async (ctx) => {
  const id = ctx.params.id;

  const exists = await getAccountById(ctx, { id });
  if (!exists) {
    return ctx.throw(404, `Account with id ${id} does not exist and cannot be deleted`);
  }

  await deleteAccount(ctx, { id });

  ctx.status = 200;
});

router.put('/:id', koaBody(), async (ctx) => {
  const id = ctx.params.id;
  const updates = ctx.request.body;

  const exists = await getAccountById(ctx, { id });
  if (!exists) {
    return ctx.throw(404, `Account with id ${id} does not exist and cannot be updated`);
  }

  const updated = await updateAccount(ctx, { ...updates, id });

  ctx.status = 200;
  ctx.body = {
    account: updated
  };
});

export default router;
