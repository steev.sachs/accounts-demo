import Router from 'koa-router';
import accounts from './accounts';

const router = new Router();

router.use('/accounts', accounts.routes(), accounts.allowedMethods());

export default router;
