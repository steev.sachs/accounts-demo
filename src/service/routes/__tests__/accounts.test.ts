import { getTestApp } from '../../test-utils/getTestApp';
import { omit } from 'ramda';
import { stringify } from 'qs';
import accountsModel from '../../models/accounts';
import mockAccounts from '../../test-utils/fixtures/mockAccounts';
import supertest from 'supertest';

let app: supertest.SuperTest<supertest.Test>;
let server: any;

beforeEach((done) => {
  server = getTestApp();
  app = supertest(server);
  done();
});

afterEach((done) => {
  server.close();
  jest.clearAllMocks();
  done();
});

describe('routes > accounts', () => {
  describe('GET /', () => {
    it('returns all accounts', async () => {
      const getAllAccountsSpy = jest
        .spyOn(accountsModel, 'getAllAccounts')
        .mockResolvedValue(mockAccounts);

      const { body, status } = await app.get('/accounts');
      expect(status).toEqual(200);
      expect(body).toEqual({ accounts: JSON.parse(JSON.stringify(mockAccounts)) });
      expect(getAllAccountsSpy).toHaveBeenCalledWith(expect.any(Object));
    });

    it.each([
      ['any', { any: 'test' }, mockAccounts],
      ['name', { name: 'one' }, [mockAccounts[0]]],
      ['emailAddress', { emailAddress: 'test2' }, [mockAccounts[1]]],
      ['id', { id: mockAccounts[2].id }, [mockAccounts[2]]],
      [
        'multiple parameters',
        { any: 'test', name: 'one', emailAddress: 'test2' },
        [mockAccounts[1], mockAccounts[0]]
      ]
    ])('searches by %s', async (_: string, params: object, expected: any) => {
      const getAllAccountsSpy = jest
        .spyOn(accountsModel, 'getAllAccounts')
        .mockResolvedValue(mockAccounts);

      const { body, status } = await app.get(
        `/accounts${stringify(params, { addQueryPrefix: true })}`
      );
      expect(status).toEqual(200);
      expect(body).toEqual({ accounts: JSON.parse(JSON.stringify(expected)) });
      expect(getAllAccountsSpy).toHaveBeenCalledWith(expect.any(Object));
    });
  });

  describe('GET /:id', () => {
    it('gets an account', async () => {
      const getAllAccountsSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(mockAccounts[0]);

      const id = mockAccounts[0].id;

      const { body, status } = await app.get(`/accounts/${id}`);
      expect(status).toEqual(200);
      expect(body).toEqual({ account: JSON.parse(JSON.stringify(mockAccounts[0])) });
      expect(getAllAccountsSpy).toHaveBeenCalledWith(expect.any(Object), { id });
    });

    it('returns 404 if account does not exist', async () => {
      const getAccountByIdSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(undefined);

      const id = mockAccounts[0].id;

      const { body, status } = await app.get(`/accounts/${id}`);
      expect(status).toEqual(404);
      expect(body).toEqual({ error: `No account found with id ${id}` });
      expect(getAccountByIdSpy).toHaveBeenCalledWith(expect.any(Object), { id });
    });
  });

  describe('DELETE /:id', () => {
    it('deletes an account', async () => {
      const getAccountByIdSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(mockAccounts[0]);

      const deleteAccountSpy = jest
        .spyOn(accountsModel, 'deleteAccount')
        .mockResolvedValue(mockAccounts.slice(1));

      const id = mockAccounts[0].id;

      const { status } = await app.delete(`/accounts/${id}`);
      expect(status).toEqual(200);
      expect(getAccountByIdSpy).toHaveBeenCalledWith(expect.any(Object), { id });
      expect(deleteAccountSpy).toHaveBeenCalledWith(expect.any(Object), { id });
    });

    it('returns 404 if account does not exist', async () => {
      const deleteAccountSpy = jest.spyOn(accountsModel, 'deleteAccount').mockImplementation(() => {
        throw new Error('should not have been called');
      });

      const getAccountByIdSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(undefined);

      const id = mockAccounts[0].id;

      const { body, status } = await app.delete(`/accounts/${id}`);
      expect(status).toEqual(404);
      expect(body).toEqual({ error: `Account with id ${id} does not exist and cannot be deleted` });
      expect(getAccountByIdSpy).toHaveBeenCalledWith(expect.any(Object), { id });
      expect(deleteAccountSpy).not.toHaveBeenCalled();
    });
  });

  describe('POST /', () => {
    it('inserts an account', async () => {
      const insertAccountSpy = jest
        .spyOn(accountsModel, 'insertAccount')
        .mockResolvedValue(mockAccounts[0]);

      const { id, ...payload } = mockAccounts[0];

      const { body, status } = await app.post('/accounts').send(payload);
      expect(body).toEqual({ account: JSON.parse(JSON.stringify(mockAccounts[0])) });
      expect(status).toEqual(200);
      expect(insertAccountSpy).toHaveBeenCalledWith(expect.any(Object), { account: payload });
    });

    it.each(['birthday', 'emailAddress', 'name'])(
      'throws an error when %s is undefined',
      async (undefinedProp: string) => {
        const insertAccountSpy = jest
          .spyOn(accountsModel, 'insertAccount')
          .mockImplementation(async () => {
            throw new Error('should not have been called');
          });

        const { id, ...payload } = mockAccounts[0];
        const errorPayload = omit([undefinedProp], payload);

        const { body, status } = await app.post('/accounts').send(errorPayload);
        expect(body).toEqual({ error: 'birthday, emailAddress, and name are required properties' });
        expect(status).toEqual(400);
        expect(insertAccountSpy).not.toHaveBeenCalled();
      }
    );
  });

  describe('PUT /:id', () => {
    it('updates an account', async () => {
      const id = mockAccounts[0].id;
      const updates = {
        name: 'test'
      };
      const updated = { ...mockAccounts[0], ...updates };

      const getAccountByIdSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(mockAccounts[0]);

      const updateAccountSpy = jest
        .spyOn(accountsModel, 'updateAccount')
        .mockResolvedValue(updated);

      const { body, status } = await app.put(`/accounts/${id}`).send(updates);
      expect(body).toEqual({ account: JSON.parse(JSON.stringify(updated)) });
      expect(status).toEqual(200);
      expect(getAccountByIdSpy).toHaveBeenCalledWith(expect.any(Object), { id });
      expect(updateAccountSpy).toHaveBeenCalledWith(expect.any(Object), { ...updates, id });
    });

    it('returns 404 if account does not exist', async () => {
      const updateAccountSpy = jest.spyOn(accountsModel, 'deleteAccount').mockImplementation(() => {
        throw new Error('should not have been called');
      });

      const getAccountByIdSpy = jest
        .spyOn(accountsModel, 'getAccountById')
        .mockResolvedValue(undefined);

      const id = mockAccounts[0].id;

      const { body, status } = await app.put(`/accounts/${id}`).send({});
      expect(status).toEqual(404);
      expect(body).toEqual({ error: `Account with id ${id} does not exist and cannot be updated` });
      expect(getAccountByIdSpy).toHaveBeenCalledWith(expect.any(Object), { id });
      expect(updateAccountSpy).not.toHaveBeenCalled();
    });
  });
});
