import { Middleware } from 'koa';

const handleError: Middleware = async (ctx, next) => {
  try {
    await next();
  } catch ({ body, message, status = 500 }) {
    ctx.status = status;

    // Mask server errors in production
    if (process.env.NODE_ENV !== 'production') {
      ctx.body = body
        ? body
        : {
            error: message
          };
    }
  }
};

export default handleError;
