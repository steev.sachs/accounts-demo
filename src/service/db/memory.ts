import cuid from 'cuid';
import { User } from '../../types';

const db: User[] = [
  {
    birthday: new Date('1986-12-31'),
    emailAddress: 'test1@test.com',
    id: '' + cuid(),
    name: 'Tester One'
  },
  {
    birthday: new Date('1984-02-12'),
    emailAddress: 'test2@test.com',
    id: '' + cuid(),
    name: 'Tester Two'
  },
  {
    birthday: new Date('1989-10-31'),
    emailAddress: 'test3@test.com',
    id: '' + cuid(),
    name: 'Tester Three'
  },
  {
    birthday: new Date('1990-11-25'),
    emailAddress: 'test4@test.com',
    id: '' + cuid(),
    name: 'Tester Four'
  },
  {
    birthday: new Date('1986-01-01'),
    emailAddress: 'test5@test.com',
    id: '' + cuid(),
    name: 'Tester Five'
  }
];

export default db;
