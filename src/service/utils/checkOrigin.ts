import { Context } from 'koa';
import { find, path } from 'ramda';

const checkOrigin = (ctx: Context) => {
  const corsDomains: RegExp[] = [/^https?:\/\/localhost(:\d+)?$/];

  const origin = path(['accept', 'headers', 'origin'], ctx) as string;
  const found = find((domain: RegExp) => domain.test(origin), corsDomains);
  if (!found) {
    return ctx.throw(401);
  }

  return origin;
};

export default checkOrigin;
