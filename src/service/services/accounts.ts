import { Ctx, User } from '../../types';
import { eqBy, filter, mapObjIndexed, prop, unionWith, values } from 'ramda';
import accountsModel from '../models/accounts';

const getAccountById = async (ctx: Ctx, { id }: { id: string }) =>
  accountsModel.getAccountById(ctx, { id });

const deleteAccount = async (ctx: Ctx, { id }: { id: string }) =>
  accountsModel.deleteAccount(ctx, { id });

const getAllAccounts = async (ctx: Ctx) => accountsModel.getAllAccounts(ctx);

const insertAccount = async (ctx: Ctx, { account: { birthday, ...rest } }: { account: User }) => {
  const params = {
    account: {
      birthday: new Date(birthday),
      ...rest
    }
  };
  const inserted = await accountsModel.insertAccount(ctx, params);

  return inserted;
};

const searchAccounts = async (ctx: Ctx, params: Partial<User> & { any?: string }) => {
  // in a real db implementation, a query should be constructed rather than locally filtering
  const accounts = await getAllAccounts(ctx);
  const matches = mapObjIndexed(
    (query: string, key: string) =>
      filter((account: User) => {
        return key === 'any'
          ? new RegExp(query, 'ig').test(JSON.stringify(account))
          : new RegExp(query, 'ig').test(prop(key, account));
      }, accounts),
    params
  );

  const { any, ...rest } = matches;
  if (Object.keys(rest).length > 0) {
    return values(rest).reduce(unionWith(eqBy(prop('id'))), []);
  } else {
    return any;
  }
};

const updateAccount = async (ctx: Ctx, params: Partial<User> & { id: string }) =>
  accountsModel.updateAccount(ctx, params);

export {
  deleteAccount,
  getAccountById,
  getAllAccounts,
  insertAccount,
  searchAccounts,
  updateAccount
};
