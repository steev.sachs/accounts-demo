import { MouseEventHandler } from 'react';

type Events = Partial<{
  onClick: MouseEventHandler;
  onMouseDown: MouseEventHandler;
  onMouseEnter: MouseEventHandler;
  onMouseLeave: MouseEventHandler;
  onMouseUp: MouseEventHandler;
}>;

export type IconProps = Partial<{
  className: string;
  height: string | number;
  preserveAspectRatio: string;
  title: string;
  width: string | number;
}> &
  Events;
