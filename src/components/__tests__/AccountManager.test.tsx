import { fireEvent, render, wait, waitForElement } from 'react-testing-library';
import AccountManager from '../AccountManager';
import React from 'react';
import mockAccounts from '../../../service/test-utils/fixtures/mockAccounts';

jest.mock('react-virtualized-auto-sizer', () => ({ children }) => (
  <div>{children({ height: 100, width: 100 })}</div>
));

beforeEach(() => {
  fetch.resetMocks();
});

const originalError = console.error;
beforeAll(() => {
  console.error = (...args) => {
    if (/Warning.*not wrapped in act/.test(args[0])) {
      return;
    }
    originalError.call(console, ...args);
  };
});

afterAll(() => {
  console.error = originalError;
});

describe('<AccountManager />', () => {
  it('renders', async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        accounts: mockAccounts
      })
    );
    const { getByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
  });

  it('creates', async () => {
    fetch
      .once(
        JSON.stringify({
          accounts: mockAccounts
        })
      )
      .once(
        JSON.stringify({
          account: {
            id: 'test-id',
            birthday: new Date('2020-01-01'),
            emailAddress: 'test-email',
            name: 'test-name'
          }
        })
      );
    const { container, getByText, getAllByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    fireEvent.click(getByText('+ Create Account'));
    await wait();
    fireEvent.change(container.querySelectorAll('input')[1], { target: { value: 'test-name' } });
    fireEvent.change(container.querySelectorAll('input')[2], {
      target: { value: 'test-email' }
    });
    fireEvent.change(container.querySelectorAll('input')[3], { target: { value: '2020-01-01' } });
    fireEvent.click(getAllByText('Save')[0]);
    await waitForElement(() => getByText('test-id'));
    expect(container.querySelectorAll('input').length).toBe(0);
  });

  it('deletes', async () => {
    fetch
      .once(
        JSON.stringify({
          accounts: mockAccounts
        })
      )
      .once();
    const { getByText, getAllByText, queryByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    fireEvent.click(getAllByText('Delete this account')[0]);
    await wait();
    expect(queryByText(mockAccounts[0].id)).not.toBeInTheDocument();
  });

  it('errors on delete', async () => {
    fetch.once(
      JSON.stringify({
        accounts: mockAccounts
      })
    );
    fetch.mockReject();
    const { getByText, getAllByText, queryByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    fireEvent.click(getAllByText('Delete this account')[0]);
    await waitForElement(() => getByText('Failed to delete'));
    expect(getByText(mockAccounts[0].id)).toBeInTheDocument();
  });

  it('edits', async () => {
    fetch
      .once(
        JSON.stringify({
          accounts: mockAccounts
        })
      )
      .once();
    const { container, getByText, getAllByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    fireEvent.click(getAllByText('Edit account')[0]);
    await wait();
    fireEvent.change(container.querySelectorAll('input')[1], { target: { value: 'testertron' } });
    fireEvent.click(getAllByText('Save')[0]);
    expect(container.querySelectorAll('input').length).toBe(0);
    expect(getByText('testertron')).toBeInTheDocument();
  });

  it('errors on edit failure', async () => {
    fetch.once(
      JSON.stringify({
        accounts: mockAccounts
      })
    );
    fetch.mockReject(new Error('test'));
    const { container, getByText, getAllByText, queryByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    fireEvent.click(getAllByText('Edit account')[0]);
    await wait();
    fireEvent.change(container.querySelectorAll('input')[1], { target: { value: 'testertron' } });
    fireEvent.click(getAllByText('Save')[0]);
    await waitForElement(() => getByText('Failed to save'));
    expect(container.querySelectorAll('input').length).toBe(0);
    expect(queryByText('testertron')).not.toBeInTheDocument();
  });

  it('cancels edit', async () => {
    fetch.once(
      JSON.stringify({
        accounts: mockAccounts
      })
    );
    const { container, getByText, getAllByText, queryByText } = render(<AccountManager />);
    await waitForElement(() => getByText(mockAccounts[0].id));
    expect(getByText(mockAccounts[0].name)).toBeInTheDocument();
    fireEvent.click(getAllByText('Edit account')[0]);
    await wait();
    fireEvent.change(container.querySelectorAll('input')[1], { target: { value: 'testertron' } });
    fireEvent.click(getAllByText('Cancel')[0]);
    expect(container.querySelectorAll('input').length).toBe(0);
    expect(queryByText('testertron')).not.toBeInTheDocument();
    expect(getByText(mockAccounts[0].name)).toBeInTheDocument();
  });

  it('renders error', async () => {
    fetch.mockReject(new Error('test error'));
    const { getByText } = render(<AccountManager />);
    await waitForElement(() => getByText('Error fetching data'));
  });
});
