import { css } from 'emotion';
import ContentLoader from 'react-content-loader';
import React from 'react';

const styles = {
  container: css({
    alignItems: 'center',
    display: 'flex'
  })
};

const LoadingRow = ({ style }: { style: object }) => (
  <ContentLoader className={styles.container} style={style}>
    <rect
      x={20}
      y={50}
      rx="2"
      ry="2"
      height="5"
      style={{ height: '1.5em', width: '90%' }}
    />
  </ContentLoader>
);

export default LoadingRow;
