import { FixedSizeList, ListChildComponentProps } from 'react-window';
import Autosizer from 'react-virtualized-auto-sizer';
import React from 'react';

type TableProps = { itemCount: number };

const Table: React.FC<TableProps> = ({ children, itemCount }) => (
  <Autosizer>
    {({ height, width }) => (<FixedSizeList height={height} itemCount={itemCount || 1000} itemSize={90} width={width}>
      {children as React.ComponentClass<ListChildComponentProps>}
    </FixedSizeList>)}
  </Autosizer>
);

export default Table;
