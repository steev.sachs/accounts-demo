import { Action, HttpContent, HttpStatus, User } from '../types';
import { css } from 'emotion';
import { eqBy, prop, propEq, reject, unionWith } from 'ramda';
import { makeTableRow } from './TableRow';
import CreateAccount from './CreateAccount';
import LoadingRow from './LoadingRow';
import React, { useEffect, useReducer, useState } from 'react';
import SearchInput from './SearchInput';
import Table from './Table';
import config from '../config';

type AccountManagerProps = {};

type AccountsState = HttpContent<User[]>;

const styles = {
  button: css({
    '& > button': {
      background: '#2b2b2b',
      border: 'none',
      borderRadius: 2,
      color: '#FFF',
      cursor: 'pointer',
      marginRight: '2em',
      padding: '1em'
    },
    display: 'flex',
    justifyContent: 'flex-end',
    padding: '1.55em 0',
    width: '100%'
  }),
  container: css({
    height: 'calc(90vh - 200px)',
    minHeight: 400,
    width: '100vw'
  }),
  header: css({
    display: 'flex',
    justifyContent: 'center',
  }),
  main: css({
    width: '100%'
  })
};

const initialState: HttpContent<User> = {
  type: HttpStatus.NotAsked
};

const reducer = (state: AccountsState, action: Action) => {
  switch (action.type) {
    case 'accounts/ADD_ONE': {
      if (state.type !== HttpStatus.Success) {
        return state;
      }

      const data = unionWith(eqBy(prop('id')), [action.payload as User], state.data);

      return {
        ...state,
        data
      };
    }
    case 'accounts/DELETE_ONE': {
      if (state.type !== HttpStatus.Success) {
        return state;
      }

      return { ...state, data: reject(propEq('id', action.payload), state.data) };
    }
    case 'accounts/ERROR':
      return { message: action.payload, type: HttpStatus.Error } as AccountsState;
    case 'accounts/FETCH':
      return { type: HttpStatus.Fetching } as AccountsState;
    case 'accounts/SUCCESS':
      return { data: action.payload as User[], type: HttpStatus.Success } as AccountsState;
    default:
      return state as AccountsState;
  }
};

const AccountManager: React.FC<AccountManagerProps> = () => {
  const [showCreateAccount, setShowCreateAccount] = useState<boolean>(false);
  const [state, dispatch] = useReducer(reducer, initialState);
  const fetchData = async () => {
    try {
      const response = await fetch(`${config.serviceUrl}/accounts`);
      const { accounts } = await response.json();
      dispatch({ payload: accounts, type: 'accounts/SUCCESS' });
    } catch (e) {
      dispatch({ payload: 'Error fetching data', type: 'accounts/ERROR' });
    }
  };
  useEffect(() => {
    if (state.type === HttpStatus.NotAsked) {
      dispatch({ type: 'accounts/FETCH' });
      fetchData();
    }
  }, []);

  const createAccountDispatch = (action: Action) => {
    setShowCreateAccount(false);
    dispatch(action);
  };

  const CreateButton = () => (
    <div className={styles.button}>
      <button onClick={() => setShowCreateAccount(true)}>+ Create Account</button>
    </div>
  );

  return state.type === HttpStatus.Success ? (
    <div className={styles.main}>
      <div className={styles.container}>
        <h1 className={styles.header}>Account Manager</h1>
        <SearchInput dispatch={dispatch} />
        {showCreateAccount ? <CreateAccount dispatch={createAccountDispatch} /> : <CreateButton />}
        {state.data.length > 0 ? (
          <Table itemCount={state.data.length}>
            {makeTableRow({ data: state.data, dispatch })}
          </Table>
        ) : (
          <div>No results</div>
        )}
      </div>
    </div>
  ) : state.type === HttpStatus.NotAsked || state.type === HttpStatus.Fetching ? (
    <div className={styles.main}>
      <h1 className={styles.header}>Account Manager</h1>
      <div className={styles.container}>
        {showCreateAccount ? <CreateAccount dispatch={createAccountDispatch} /> : <CreateButton />}
        <Table itemCount={3}>{LoadingRow}</Table>
      </div>
    </div>
  ) : (
    <div>{state.message}</div>
  );
};

export default AccountManager;
