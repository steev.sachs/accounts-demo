import { Action } from '../types';
import { css } from 'emotion';
import { filter } from 'ramda';
import { stringify } from 'qs';
import { useDebouncedCallback } from 'use-debounce';
import React, { useState } from 'react';
import config from '../config';

type SearchInputProps = {
  dispatch: React.Dispatch<Action>;
};

const styles = {
  button: css({
    background: '#2b2b2b',
    border: 'none',
    borderRadius: 2,
    color: '#FFF',
    cursor: 'pointer',
    marginRight: '1em',
    padding: '1em'
  }),
  container: css({
    '& > div': {
      alignItems: 'center',
      display: 'flex'
    },
    display: 'flex',
    justifyContent: 'space-between'
  }),
  explanation: css({
    color: '#aaa',
    margin: '.5em 1em'
  }),
  input: css({
    border: '1px solid #2b2b2b',
    borderRadius: 2,
    margin: '0 1em',
    width: '90%'
  }),
  outer: css({
    padding: '0 1em',
  })
};

const SearchInput: React.FC<SearchInputProps> = ({ dispatch }) => {
  const [anyQuery, setAnyQuery] = useState('');
  const [emailQuery, setEmailQuery] = useState('');
  const [idQuery, setIdQuery] = useState('');
  const [nameQuery, setNameQuery] = useState('');

  const search = async ({ query }: { query: { [key: string]: string } }) => {
    try {
      const response = await fetch(
        `${config.serviceUrl}/accounts${stringify(query, { addQueryPrefix: true })}`
      );
      const { accounts } = await response.json();
      dispatch({ payload: accounts, type: 'accounts/SUCCESS' });
    } catch (e) {
      dispatch({ payload: 'Failed to search', type: 'accounts/ERROR' });
    }
  };

  const [performSearch] = useDebouncedCallback((query: { [key: string]: string }) => {
    dispatch({ type: 'accounts/FETCH' });
    search({ query });
  }, 500);

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.innerText;
    const email = value.match(/@email:([A-Za-z.@0-9]*)(?=\s|$)/);
    const id = value.match(/@id:([A-Za-z0-9]*)(?=\s|$)/);
    const first = value.match(/@first:([A-Za-z]*)(?=\s|$)/);
    const last = value.match(/@last:([A-Za-z]*)(?=\s|$)/);
    email && setEmailQuery(email[1]);
    id && setIdQuery(id[1]);
    const name = `${first ? first[1] : ''}${first && last ? ' ' : ''}${last ? last[1] : ''}`;
    name && setNameQuery(name);
    setAnyQuery(value);
  };

  const handleSearch = () =>
    performSearch(
      filter(Boolean, {
        any: anyQuery,
        emailAddress: emailQuery,
        id: idQuery,
        name: nameQuery
      })
    );

  return (
    <div className={styles.outer}>
      <div className={styles.container}>
        <div
          className={styles.input}
          contentEditable={true}
          data-testid="search-input"
          onInput={handleOnChange}
        />
        <button className={styles.button} onClick={handleSearch}>
          Search
        </button>
      </div>
      <div className={styles.explanation}>
        Search accounts. You can use tokens <b>@email:</b>, <b>@first:</b>, <b>@last:</b>, and{' '}
        <b>@id:</b>.
      </div>
      <div className={styles.explanation}>
        For example: <em>@email:steev.sachs@gmail.com</em>
      </div>
    </div>
  );
};

export default SearchInput;
