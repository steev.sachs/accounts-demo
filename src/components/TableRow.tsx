import { ListChildComponentProps } from 'react-window';
import { Action, User } from '../types';
import { css } from 'emotion';
import { filter } from 'ramda';
import Check from './icons/Check';
import Close from './icons/Close';
import Pencil from './icons/Pencil';
import React, { useState } from 'react';
import config from '../config';
import WarningStandard from './icons/WarningStandard';
import Trash from './icons/Trash';
import LoadingRow from './LoadingRow';

type Dispatch = (action: Action) => void;

type TableRowProps = ListChildComponentProps;

type TableRowComponentProps = Partial<
  User & {
    dispatch: Dispatch;
    error: string;
    fill: boolean;
    handleCancel: () => void;
    handleDelete: () => void;
    style: object;
  }
>;

const styles = {
  container: css({
    '& > div': {
      flexBasis: '24%',
      overflow: 'hidden',
      padding: '1em',
      textOverflow: 'ellipsis',
      whitespace: 'nowrap'
    },
    '& > div:last-child': {
      flexBasis: '5%'
    },
    display: 'flex',
    justifyContent: 'space-between',
    padding: 19
  }),
  disabled: css({
    cursor: 'not-allowed',
    opacity: 0.4
  }),
  error: css({
    fill: 'red'
  }),
  fill: css({
    '& svg': {
      fill: '#FFF'
    },
    background: '#2b2b2b',
    color: '#FFF'
  }),
  icon: css({
    cursor: 'pointer',
    height: '1em',
    marginRight: '.5em'
  }),
  input: css({
    fontSize: '1em !important'
  }),
  redBorder: css({
    border: '1px solid red !important'
  })
};

const TableRow: React.FC<TableRowComponentProps> = ({
  birthday = new Date(),
  dispatch,
  emailAddress = '',
  error = '',
  fill = false,
  handleCancel: outerHandleCancel,
  handleDelete,
  id = '',
  name = '',
  style = {}
}) => {
  const [errorValue, setError] = useState<string>(error);
  const [idValue, setIdValue] = useState<string | undefined>(id);
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [birthdayValue, setBirthdayValue] = useState<string>(birthday.toString());
  const [emailAddressValue, setEmailAddressValue] = useState<string>(emailAddress);
  const [nameValue, setNameValue] = useState<string>(name);

  const allowSave = () => birthdayValue && emailAddressValue && nameValue;

  const getBody = ({
    birthday: currentBirthdayValue,
    emailAddress: currentEmailAddressValue,
    name: currentNameValue
  }: {
    birthday: string;
    emailAddress: string;
    name: string;
  }) =>
    filter(Boolean, {
      birthday: currentBirthdayValue !== birthday.toString() && currentBirthdayValue,
      emailAddress: currentEmailAddressValue !== emailAddress && currentEmailAddressValue,
      name: currentNameValue !== name && currentNameValue
    });

  const saveData = async () => {
    const method = idValue ? 'PUT' : 'POST';
    setIsEditing(false);
    try {
      const response = await fetch(`${config.serviceUrl}/accounts/${idValue ? idValue : ''}`, {
        body: JSON.stringify(
          getBody({
            birthday: birthdayValue,
            emailAddress: emailAddressValue,
            name: nameValue
          })
        ),
        headers: { 'content-type': 'application/json' },
        method
      });
      const { account } = await response.json();
      setIdValue(account.id);
      dispatch && dispatch({ payload: account, type: 'accounts/ADD_ONE' });
    } catch (e) {
      setError('Failed to save');
      handleCancel();
    }
  };

  const makeHandleSetValue = (setter: (newVal: string) => void) => (
    e: React.ChangeEvent<HTMLInputElement>
  ) => setter(e.target.value);

  const handleCancel = () => {
    setIsEditing(false);
    setBirthdayValue(birthday.toString());
    setEmailAddressValue(emailAddress);
    setNameValue(name);
    outerHandleCancel && outerHandleCancel();
  };

  const setEditingAndClearError = (newVal: boolean = true) => {
    setError('');
    setIsEditing(newVal);
  };

  const viewMode = (
    <div className={css(fill && styles.fill)} style={style}>
      <div className={styles.container}>
        <div>{idValue}</div>
        <div>{nameValue}</div>
        <div>{emailAddressValue}</div>
        <div>{new Date(birthdayValue).toLocaleString()}</div>
        <div>
          <Pencil
            className={styles.icon}
            onClick={() => setEditingAndClearError()}
            title="Edit account"
          />
          {errorValue && (
            <WarningStandard className={css(styles.icon, styles.error)} title={errorValue} />
          )}
          {handleDelete && (
            <Trash className={styles.icon} onClick={handleDelete} title="Delete this account" />
          )}
        </div>
      </div>
    </div>
  );

  const editMode = (
    <div className={css(fill && styles.fill)} style={style}>
      <div className={styles.container}>
        <input className={styles.input} disabled name="id" type="text" value={idValue} />
        <input
          autoFocus
          className={css(styles.input, !nameValue && styles.redBorder)}
          name="name"
          onChange={makeHandleSetValue(setNameValue)}
          type="text"
          value={nameValue}
        />
        <input
          className={css(styles.input, !emailAddressValue && styles.redBorder)}
          name="emailAddress"
          onChange={makeHandleSetValue(setEmailAddressValue)}
          type="email"
          value={emailAddressValue}
        />
        <input
          className={css(styles.input, !birthdayValue && styles.redBorder)}
          name="birthday"
          onChange={makeHandleSetValue(setBirthdayValue)}
          type="date"
          value={new Date(birthdayValue).toISOString().substr(0, 10)}
        />
        <div>
          <Close className={styles.icon} onClick={handleCancel} title="Cancel" />
          <Check
            className={css(styles.icon, !allowSave() && styles.disabled)}
            onClick={allowSave() ? () => saveData() : undefined}
            title="Save"
          />
        </div>
      </div>
    </div>
  );

  return isEditing || !idValue ? editMode : viewMode;
};

const makeTableRow = ({ data, dispatch }: { data: User[]; dispatch: Dispatch }) => ({
  index,
  style
}: TableRowProps) => {
  const [deleteError, setDeleteError] = useState<string>('');
  const [isDeleting, setIsDeleting] = useState<boolean>(false);

  const deleteRow = async () => {
    try {
      await fetch(`${config.serviceUrl}/accounts/${id}`, {
        method: 'DELETE'
      });
      dispatch({ payload: id, type: 'accounts/DELETE_ONE' });
    } catch (e) {
      setDeleteError('Failed to delete');
      setIsDeleting(false);
    }
  };

  const handleDelete = () => {
    setIsDeleting(true);
    deleteRow();
  };

  const { birthday, emailAddress, id, name } = data[index] || ({} as User);
  const fill = index % 2 !== 0;
  return isDeleting ? (
    <LoadingRow style={style} />
  ) : (
    <TableRow
      {...{
        birthday,
        emailAddress,
        error: deleteError,
        fill,
        handleDelete,
        id,
        name,
        style
      }}
    />
  );
};

export { makeTableRow };
export default TableRow;
