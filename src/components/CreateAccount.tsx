import { Action } from '../types';
import React, { Dispatch } from 'react';
import TableRow from './TableRow';

type CreateAccountProps = {
  dispatch: Dispatch<Action>;
};

const CreateAccount: React.FC<CreateAccountProps> = ({ dispatch }) => {
  const handleCancel = () => {
    dispatch({ type: 'accounts/CANCEL_CREATE' });
  };
  return <TableRow dispatch={dispatch} handleCancel={handleCancel} />;
};

export default CreateAccount;
