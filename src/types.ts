import { IRouterContext } from 'koa-router';

export type Action = {
  payload?: User[] | User | string | undefined;
  type: string;
};

export type CtxState = {};

export interface Ctx extends IRouterContext {
  state: CtxState;
}

export enum HttpStatus {
  Error = 'error',
  Fetching = 'fetching',
  NotAsked = 'not-asked',
  Success = 'success'
}

export type Error = {
  message: string
  type: HttpStatus.Error
}

export type Fetching = {
  type: HttpStatus.Fetching
}

export type MergeableFetching<T> = {
  data: T
  type: HttpStatus.Fetching
}

export type NotAsked = {
  type: HttpStatus.NotAsked
}

export type Success<T> = {
  data: T
  type: HttpStatus.Success
}

export type HttpContent<T> = NotAsked | Fetching | Success<T> | Error

export type User = {
  birthday: Date;
  emailAddress: string;
  id?: string;
  name: string;
};
